# dicom2mesh

A simple Python script which utilises marching cubes algorithm(skimage library) to generate 3D mesh from Dicom/Nifti image stack, which then can be output to .obj file.